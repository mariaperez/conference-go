from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Attendee
from events.models import Conference, State
from json import JSONEncoder
from django.views.decorators.http import require_http_methods
import json


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["name"]


class ConferenceEncoder(JSONEncoder):
    def default(self, o):
        if isinstance(o, Conference):
            return {
                'name': o.name,
                'description': o.description,
                'max_presentations': o.max_presentations,
                'max_attendees': o.max_attendees,
                'starts': o.starts.isoformat(),
                'ends': o.ends.isoformat(),
                'created': o.created.isoformat(),
                'updated': o.updated.isoformat(),
                'location': {
                    'name': o.location.name,
                    'city': o.location.city,
                    'room_count': o.location.room_count,
                    'created': o.location.created.isoformat(),
                    'updated': o.location.updated.isoformat(),
                    'state': o.location.state.abbreviation,
                },
            }
        return super().default(o)


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "email",
        "name",
        "company_name",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceEncoder(),
    }


class ShowDetailEncoder(ModelEncoder):
    model = Attendee
    properties = [
        "name",
        "email",
        "company_name",
        "created",
        "conference",
        "name",
    ]
    encoders = {
        "conference": ConferenceEncoder(),
    }


@require_http_methods(["DELETE", "GET", "PUT"])
def api_show_attendee(request, id):
    if request.method == "GET":
        attendee = Attendee.objects.get(id=id)
        return JsonResponse(
            attendee,
            encoder=ShowDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=id).delete()
        return JsonResponse(
            {
                "deleted": count > 0
            }
        )
    else:
        # copied from create
        content = json.loads(request.body)
        try:
            # new code
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )


@require_http_methods(["GET", "POST", "PUT", "DELETE"])
def api_list_attendees(request, conference_id):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference_id=conference_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        # Get the Conference object and put it in the content dict
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = Attendee.objects.filter(id=conference_id).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        # copied from create
        content = json.loads(request.body)
        try:
            if "attendee" in content:
                attendee = Attendee.objects.get(abbreviation=content["attendee"])
                content["attendee"] = attendee
        except Attendee.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid attendee abbreviation"},
                status=400,
            )
        Attendee.objects.filter(id=conference_id).update(**content)

        attendee = Attendee.objects.get(id=conference_id)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
